#include <stdio.h>
#include <stdlib.h>

typedef struct ElementAbonne {
	int num_tel;
	char *nom_abonne;
	char *adresse;
	struct ElementAbonne*precedent;
	struct ElementAbonne*suivant;
}Abonne;
typedef struct Zone {
	int code;
	char*nom_zone;
	Abonne*Debut_abonne;
	Abonne*Fin_abonne;
	int nb_abonnes;
	struct Zone*suivant;
}Repertoire_telephone;
Repertoire_telephone*Initia_Repertoire(){
	Repertoire_telephone *Head;
	Head=(Repertoire_telephone*) malloc(sizeof(Repertoire_telephone));
    Head->Debut_abonne=NULL;
    Head->Fin_abonne=NULL; 
    Head->suivant=NULL;
    Head->nb_abonnes=0;
    Head->nom_zone=(char*)malloc(20*sizeof(char));
	return Head;
}
Repertoire_telephone*Head_Zones=NULL;
int Ajouter_zone(int code_zone , char *nom_zone){
	Repertoire_telephone*New=Initia_Repertoire();
	New->code=code_zone;
	New->nom_zone=nom_zone;
	if(Head_Zones==NULL){   Head_Zones=New;  }
	else {  
	 Repertoire_telephone*temp=Head_Zones; 
	 while(temp->suivant!=NULL){   temp=temp->suivant;   }
	 temp->suivant=New;
	   }
	   return 1;
}
Abonne*Initialisation_Abonne(){
	Abonne*ini;
	ini=(Abonne*)malloc(sizeof(Abonne));
	ini->nom_abonne=(char*)malloc(20*sizeof(char));
	ini->adresse=(char*)malloc(40*sizeof(char));
	ini->precedent=NULL;
	ini->suivant=NULL;
	return ini;
}
int Ajouter_abonne(int num_tel, char *nom_abonne,char *adresse, int code_zone){
	Abonne*New=Initialisation_Abonne();
	New->num_tel=num_tel;
	New->nom_abonne=nom_abonne;
	New->adresse=adresse;
	if(Head_Zones==NULL){  printf("Il ya 0 Des Zones"); exit(1);  }
	else {
	       Repertoire_telephone*temp2=Head_Zones;
	       while(temp2->suivant!=NULL){
	         if(temp2->code==code_zone){
	 	            if(temp2->Debut_abonne==NULL){
	 	            	temp2->nb_abonnes++;
	 	            	temp2->Debut_abonne=New;
	 	            	temp2->Fin_abonne=New;
				      }
				     else {
				     	
						 New->precedent=temp2->Fin_abonne;
						 temp2->Fin_abonne->suivant=New;
						 temp2->Fin_abonne=New;
						 temp2->nb_abonnes++;
					  }
	              }
	              temp2=temp2->suivant;
	       }
	   }
	   return 1;	
}
void Create_Zones(int n){
	int i;
	int code;
	char *nom=malloc(20*sizeof(char));
	printf("Entree Les Indormations Des Zones : \n");
	for(i=1;i<=n;i++){
		printf("Zone %d : \n",i);
		printf("Code :");
		scanf("%d",&code);
		printf("Nom De Zone :");
		scanf("%s",nom);
		int a=Ajouter_zone(code,nom);
	}
}
void Create_Abonnes(int n_zones){
	int n,i,j;
	int tel;
	char *nom=malloc(20*sizeof(char));
	char *Adresse=malloc(40*sizeof(char));
	Repertoire_telephone*temp3=Head_Zones;
    for(i=1;i<=n_zones;i++){
    	printf("Entree Nobmres des Abonnes De Zone %d : ",i);
    	scanf("%d",&temp3->nb_abonnes);
    	n=temp3->nb_abonnes;
    	printf("Enree Information De Zone %d :",i);
    	for(j=1;j<=n;j++){
    		printf("Abonne %d \n",j);
    		printf("Tel : ");
    		scanf("%d",&tel);
    		printf("Nom :");
    		scanf("%s",nom);
    		printf("Adresse :");
    		scanf("%s",Adresse);
    		int a=Ajouter_abonne(tel,nom,Adresse,temp3->code);
		}
		temp3=temp3->suivant;
	}	
}
void Affiche(int n_Zones){
	int i,j;
	Abonne*tump;
	Repertoire_telephone*temp4=Head_Zones;
	for(i=1;i<=n_Zones;i++){
		tump=temp4->Debut_abonne;
		int n=temp4->nb_abonnes;
		printf("Zone %d : (Nom=%s,Code=%d) \n",i,temp4->nom_zone,temp4->code);
		for(j=1;j<=n;j++){
			printf("Abonne %d : \n",j);
			printf("Tel : %d \t",tump->num_tel);
    		printf("Nom : %s \t",tump->nom_abonne);
    		printf("Adresse : %d",tump->adresse);
    		temp4=temp4->suivant;
		}
	tump=tump->suivant;
		
	}
}
int Enregistrer_abonne(char *nom, int code_zone){
	Repertoire_telephone*temp5=Head_Zones;
	int tel;
	while(temp5->suivant!=NULL){
		if(temp5->code==code_zone) {  break;  }
		temp5=temp5->suivant;
	}
	int n=temp5->nb_abonnes;
	Abonne*tump2=temp5->Debut_abonne;
	while(tump2->suivant!=NULL){
		if(tump2->nom_abonne==nom) {  return tump2->num_tel; }
		tump2=tump2->suivant;
	}
	return -1;
}
int Enlever_abonne(int numero){
	Repertoire_telephone*temp6=Head_Zones;
	Abonne*tump3;
	while(temp6->suivant!=NULL){
		tump3=temp6->Debut_abonne;
			while(tump3->suivant!=NULL){
		      if(tump3->num_tel==numero){
			     if (tump3->suivant==NULL){   tump3=NULL; temp6->nb_abonnes++;   }
			     else {
			     	   tump3->precedent->suivant=tump3->suivant;
			     	   tump3->suivant->precedent=tump3->precedent;
			     	   temp6->nb_abonnes++; 
				      }
		     tump3=tump3->suivant;
        	}
		temp6=temp6->suivant;
	}
  }
	return 1;
}
int Enlever_zone(int code_zone){
	Repertoire_telephone*temp7=Head_Zones;
	int n=1;
	while(temp7->suivant!=NULL){
		n++;
		temp7=temp7->suivant;
	}
	int a=1;
	while(temp7->suivant!=NULL){
		if(temp7->code==code_zone) {  break;   }
		temp7=temp7->suivant;
	}
	Abonne*temp5=temp7->Fin_abonne;
	Abonne*temp6;
	while(temp5!=NULL){
		temp6=temp5;
		temp5=temp5->precedent;
		free(temp6);
	}
	int i;
	temp7=Head_Zones;
    for(i=1;i<a;i++){
      temp7=temp7->suivant;	
	}
	temp7->suivant=temp7->suivant->suivant;
	return 1;
}

int main(int argc, char *argv[]) {
	int n,a,tel,code;
	char *nom=malloc(20*sizeof(char));
	printf("Entree Nombre Des Zones :");
	scanf("%d",&n);
	Create_Zones(n);
	Create_Abonnes(n);
	Affiche(n);
	printf("Pour Verefie : Entree Code De Zone : ");
	scanf("%d",&a);
	printf("Entree Nom De personne : ");
	scanf("%s",nom);
	int b=Enregistrer_abonne(nom,a);
	if(b=-1){  printf("La Personne N'e Pas Existe") ; }
	else { printf("Il Exist est Son Tel=%d",b); }
	printf("Entree Un Tel De Un Abonne Pour Enlever :");
	scanf("%d",&tel);
	int c=Enlever_abonne(tel);
	Affiche(n);
	printf("Entree Un Code Zone Pour  Enlever :");
	scanf("%d",&code);
	int d=Enlever_zone(code);
	return 0;
}
